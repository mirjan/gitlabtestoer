# Feature Guide


---
# GitLab Markdown

Zusammenfassung der wesentlichen Elemente für Texte aus der offiziellen Dokumentation [GitLab Markdown](https://docs.gitlab.com/ee/user/markdown.html)

## Überschriften

Markdown
```
# Überschrift 1
## Überschrift 2
### Überschrift 3
```

Ausgabe
# Überschrift 1
## Überschrift 2
### Überschrift 3


## Absätze

Markdown
```
Eine Zeile ist bis zum Zeilenumbruch ein Absatz.
Ohne Lehrzeile.

Mit Lehrzeile.
```

Ausgabe

Eine Zeile ist bis zum Zeilenumbruch ein Absatz.
Ohne Lehrzeile.

Mit Lehrzeile.

## Formatierungen

### Fett

Markdown

```
**Fettschrift**
```

Ausgabe

**Fettschrift**

### Italic

Markdown

```
*Italicschrift*
```

Ausgabe

*Italicschrift*


## Zitate

Markdown

```
> Ein Zitat
```

Ausgabe

> Ein Zitat

## Listen

### Ohne Nummern

Markdown

```
* Liste 1
* Liste 2
* Liste 3
```

Ausgabe

* Liste 1
* Liste 2
* Liste 3

### Mit Nummern

Markdown

```
1. Liste 1
1. Liste 2
1. Liste 3
```

Ausgabe

1. Liste 1
1. Liste 2
1. Liste 3

## Links

Markdown

```
[Gitlab](http://gitlab.com)
```

Ausgabe

[Gitlab](http://gitlab.com)

## Bilder

Markdown

```
![Beschreibung](http://placehold.it/100x100)
```

Ausgabe

![Beschreibung](http://placehold.it/100x100)

## Videos

Markdown
```
```

Ausgabe

## Tabellen

Markdown
```
| header 1 | header 2 | header 3 |
| ---      |  ------  |---------:|
| cell 1   | cell 2   | cell 3   |
| cell 4 | cell 5 is longer | cell 6 is much longer than the others, but that's ok. It will eventually wrap the text when the cell is too large for the display size. |
| cell 7   |          | cell <br> 9 |
```

Ausgabe

| header 1 | header 2 | header 3 |
| ---      |  ------  |---------:|
| cell 1   | cell 2   | cell 3   |
| cell 4 | cell 5 is longer | cell 6 is much longer than the others, but that's ok. It will eventually wrap the text when the cell is too large for the display size. |
| cell 7   |          | cell <br> 9 |

## Formeln

Markdown
```
\```math
a^2+b^2=c^2
\```
```

Ausgabe

```math
a^2+b^2=c^2
```

## Quellcode

Markdown
```
```

Ausgabe

## Diagramme

Markdown
```
```

Ausgabe
