{::options parse_block_html="true" /}

Test Alert-Info Box
{: .alert .alert-info}

> Noch keine Lösung dafür gefunden. Ist wahrscheinlich zu aufwendig für OER

# TestFile
---

## Table of contents

[[_TOC_]]

## Features

### Backgorund coloring:

`only in one line possible`

```
multi lines
as code-blocks
```

[+green=correct+]

[-red=false-]


### Checkbox:

- [x] Completed task
- [ ] Incomplete task
  - [ ] Sub-task 1
  - [x] Sub-task 2
  - [ ] Sub-task 3

### Collapse:

<details>
  <summary markdown="span">Solution down here.</summary>

    Great, you found the solution!
    
</details>

### Multimedia

#### Picture:

![Sample Picture per img-ordner](img/markdown_logo.png "per img-ordner")

![Sample Picture per Link](https://docs.gitlab.com/ce/user/img/markdown_logo.png "per Link")

#### Video:

![Sample Video](img/markdown_video.mp4 "GitLab sample video")

#### Audio:

![Sample Audio](img/markdown_audio.mp3 "GitLab sample audio")

### Table:

|    Features     | GitLab       | GitHub          | Moodle         |
|:----------------|:------------:|----------------:|----------------|
| Tables          | check        |                 |                |
| Collapse        |              |               a |                |
| ...             |              |                 |                |

### Lists

1. First
1. Second
   1. ordered
   1. sub-list
1. Third
   - unordered
   - sub-list
1. Last one

### Code-Blocks:

**In-line:** 
`Example` code block in-line.

**Fenced:**
```python
def hello
    print("Hello World!")
end
```
### Footnote:

Sample footnote: [^1]

[^1]: This is how a footnote looks like.

### Links:

Random sample [Link](https://www.youtube.com/embed/enMumwvLAug)

This link leads to the [Physik Course](./PhysikCourse.md "This link takes you to the Physik Course!").

This link leads to the section [end](PhysikCourse.md#end) in the Physik Course.

This link leads to the section [Table of contents](#table-of-contents) in this file.

Another link to the [PhysikCourse](PhysikCourse.md)

### User Input:

?

### Formulas:

```math
\sum_{i=1}^{n}
\Phi(x_i^2)
```

```math
\alpha*3=42
```

### Schriftgröße

<h1> Dieser Text ist riesig</h1>

<h6> Dieser Text ist winzig
  <br>und geht über mehrere Zeilen
</h6>
