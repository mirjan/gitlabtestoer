# Anleitung für OER-Kurse

Dies ist eine Anleitung zum Erstellen von offenen und frei lizenzierten Kursen, die ansprechende, und je nach Vorlage, multimediale Ergebnisse liefern und zusätzlich auch viele aufwendige Arbeiten bei der Erstellung automatisiern. Diese Anleitung kann für das Erstellen von Kursen oder Modulen ebenso genutzt werden, wie für andere Texte, wie z.B. Studien- oder Masterarbeiten.

* generiert Metadaten in HTML-Header für OER Repositories and Google Search
* ergänzt Lizenzhinweise nach TULLU-Regel für Wikimedia-Bilder mit maschinenlesbaren Hinweisen nach CC REL automatisch
* fügt Lizenzhinweis in generierte Dokumente ein

Mit jedem Speichern (Commit) werden die folgenden Dokumente generiert

* [Kurs als Ebook](https://lennart-rosseburg.gitlab.io/gitlabtestoer/course.epub)
* [Kurs als PDF](https://lennart-rosseburg.gitlab.io/gitlabtestoer/course.pdf)
* [Kurs als HTML](https://lennart-rosseburg.gitlab.io/gitlabtestoer/index.html)


# Anleitung zum Erstellen eines eigenen Kurses
### Überblick
* Ein neues Projekt anlegen 
* Metadaten und Links anpassen
* Kursinhalt bearbeiten

Zum Erstellen eines eigenen OER-Kurses wird eine dafür öffentlich zur Verfügung gestellte [Vorlage](https://gitlab.com/TIBHannover/oer/course-metadata-test) geklont und weiter verwendet. Doch zuallererst muss ein neues Projekt auf GitLab erstellt werden.

#### 1. Ein neues Projekt anlegen
* Sind Sie in Ihrem GitLab-Account angemeldet erhalten Sie [hier](https://gitlab.com) einen Überblick über Ihre bereits existierenden Projekte. Falls Sie noch keinen GitLab-Account besitzen, müssen Sie sich zunächst noch [registrieren](https://gitlab.com/users/sign_in). 

![Abbildung 1: GitLab Project Overview](img/GitLab_ProjectOverview.png "Abbildung 1: GitLab Project Overview")

* Um ein neues Projekt zu erstellen klicken Sie auf den grünen Button "New project".

![Abbildung 2: New Project Button](img/GitLab_NewProjectButton.png "Abbildung 2: New Project Button")

* Dieser Button führt Sie auf folgende Seite, in der die Option "Import Project" auszuwählen ist.

![Abbildung 3: Import Project](img/GitLab_ImportProject.png "Abbildung 3: Import Project")

* Fügen Sie nun im Feld "Git repository URL" die URL des zu klonenden Repository ein: "https://gitlab.com/TIBHannover/oer/course-metadata-test.git". Geben Sie danach ihrem Projekt noch einen Namen (frei wählbar), stellen Sie das "Visibility Level" auf "Public" und klicken Sie abschließend auf "Create project".

![Abbildung 4: Repository URL](img/GitLab_GitRepositoryUrl.png "Abbildung 4: Repository URL")

![Abbildung 5: Make Repository Public](img/GitLab_Public.png "Abbildung 5: Make Repository Public")

* Im folgenden sollte sich das Projekt erstellen und dabei das angegebene Repository erfolgreich importieren.

![Abbildung 6: Importing Project](img/GitLab_ImportProgress.png "Abbildung 6: Importing Project")

![Abbildung 7: Successfull Import](img/GitLab_SuccessfullImport.png "Abbildung 7: Successfull Import")


---
#### 2. Metadaten und Links anpassen

Nachdem das Projekt erfolgreich erstellt wurde müssen noch die Metadaten und Links vom geklonten Projekt an das eigene Projekt angepasst werden.

* Für das anpassen der Metadaten bietet sich dieser [Metadaten-Generator](https://tibhannover.gitlab.io/oer/course-metadata-gitlab-form/metadata-generator.html) an.

* Füllen Sie das Formular vom [Metadaten-Generator](https://tibhannover.gitlab.io/oer/course-metadata-gitlab-form/metadata-generator.html) mit den Informationen für Ihren Kurs aus und klicken sie unten auf "Generieren". Kopieren Sie die generierten YAML-Metadaten anschließend in die "metadata.yml", indem Sie dort die vorhandenen Metadaten vollständig ersetzen. Die "metadata.yml" finden Sie im Repository ihres Projektes.

![Abbildung 8: Metadaten-Generator](img/GitLab_MetadataGenerator.png "Abbildung 8: Metadaten-Generator")

* Sind die Metadaten aktualisiert fehlt nur noch das anpassen der Links innerhalb der "README.md". Diese Datei finden Sie ebenfalls im Repository Ihres Projektes. Öffnen Sie die Datei und klicken Sie im folgendem Fenster auf "Edit".

![Abbildung 9: Edit README.md ](img/GitLab_ReadmeEdit.png "Abbildung 9: Edit README.md")

* Im Bearbeitungsfenster müssen Sie nun den grün markierten Teil der Links mit dem folgenden Format an Ihr eigenes Projekt anpassen: "https://<user_name>.gitlab.io/<project_name>". Die Adresse der Ausgabedateien finden Sie ansonsten auch unter "SETTINGS" -> "PAGES" -> "Your pages are served under:". Den Rest der "README.md" können Sie zusätzlich nach belieben verändern.

![Abbildung 10: Update links](img/GitLab_ReadmeLinks.png "Abbildung 10: Update links")


---
#### 3. Kursinhalt bearbeiten

Nun können Sie Ihren Kursinhalt nach belieben innerhalb der "course.md" bearbeiten. Dabei werden bei jedem neuen Speichern (Commit) alle Dokumente automatisch aktualisiert und neu generiert. Für das individuelle Gestalten des Kurses bieten sich viele verschieden Möglichkeiten und Features an. Ein Guide zu allen Features finden sie [hier](https://gitlab.com/lennart-rosseburg/gitlabtestoer/FeatureGuide.md).

Beim ersten Durchlauf kann es bis zu ca. 15min dauern, bis alle Dateien generiert sind (solange gibt es einen 404). Weitere Änderungen stehen i.d.R. nach <1min bereit.


# Lizenzhinweis

Diese Vorlage für OER Kurse ist freigegeben unter CC-0 / Public domain. Die Inhalte des Kurses unterliegen der jeweiligen Lizenz, wie sie am Ende der generierten Dateien bzw. in der metadata.yml angegeben sind.
