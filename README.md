# Gitlab für Texte

... in Schule, Studium und Wissenschaft!

## Warum und wie sieht das aus/wie fühlt sich das an?

[Ein kurzes Video als Einführung](https://youtu.be/i8MxZU9w6bc)


## Klingt gut? - Hier gehts zum Kurs in [HTML](https://lennart-rosseburg.gitlab.io/gitlabtestoer/index.html), [PDF](https://lennart-rosseburg.gitlab.io/gitlabtestoer/course.pdf) oder als [EPUB](https://lennart-rosseburg.gitlab.io/gitlabtestoer/course.epub)
