# 3.1.1 Definition und Messung der Kraft

## Basiswissen „Masse der Körper und Gewichtskraft"

Eine Eigenschaft, die jeden Körper kennzeichnet, ist seine Masse. Schon seit langer Zeit werden Massen bestimmt. Mit Hilfe von Balkenwaagen konnten die Ägypter vor mehr als 5000 Jahren Massen vergleichen und unbekannte Massen bestimmen. Sind zwei Massen gleich groß, ist eine Balkenwaage bei einer Messung im Gleichgewicht. Ist die Masse auf einer Seite größer, bewegt sich diese Seite nach unten. Dieses Verhalten ist unabhängig davon, wo die Messung durchgeführt wird. Eine Anordnung, die auf der Erde im Gleichgewicht ist, wäre sogar auf dem Mond im Gleichgewicht.

> Die **Masse** ist eine Eigenschaft eines Körpers. Bringt man einen Körper zum Beispiel von der Erde auf den Mond, bleibt seine Masse erhalten. Die Einheit der Masse ist: $`[m]=kg`$.

![Abbildung 10](https://lx3.mint-kolleg.kit.edu/onlinekursphysik/html/1.3.1/Physikkurs/kraefte_definitionmessung/images/WaageGGundnGG.png "Abbildung 10: Waage im Gleichgewicht und im Nichtgleichgewicht (C)")<center>*Abbildung 10: Waage im Gleichgewicht und im Nichtgleichgewicht (C)*</center>

In der Abbildung links ist eine Balkenwaage gezeigt. Die beiden Körper auf den Schalen sind gleich schwer und die Waage ist im Gleichgewicht. Legt man jetzt auf einer Seite einen zweiten Körper dazu, bewegt sich diese Seite nach unten. Dies ist im Bild rechts gezeigt. Der Grund für diesen Vorgang liegt darin, dass sich Körper gegenseitig anziehen. Diese Anziehung hängt von der Masse ab. Hier zieht die Erde die Waage an und damit auch die beiden Waagschalen. Die Waagschale mit zwei Massen wird stärker angezogen.

> Unter der Gewichtskraft versteht man die Kraft, mit der ein Körper der Masse $`m`$ im Schwerefeld der Erde angezogen wird. Diese Kraft ist immer in Richtung zum Erdmittelpunkt gerichtet.

Diese Schwerkraft ist auch die Ursache für die Fallbeschleunigung. Jeder Körper erfährt eine Gewichtskraft. Diese Tatsache spürt jeder, der etwas trägt. Hält man zum Beispiel ein Kiste Sprudel, so spürt man, dass man dazu eine Kraft aufwenden muss. Man merkt, dass an der Kiste eine Kraft nach unten zieht. Das zeigt sich auch, wenn man sie loslässt. Sie fällt auf den Boden.

> Für die Kraft $`F`$ bzw. $`\vec{F}`$, die auf einen Körper der Masse $`m`$ im Schwerefeld der Erde wirkt, gilt: $`F=m*g`$ bzw. $`\vec{F}=m*\vec{g}`$

Hier steht $`\vec{g}`$ für die bekannte Erdbeschleunigung. Die Erdbeschleunigung hängt vom Ort ab. So erfährt ein Körper am Äquator eine andere beschleunigende Kraft als auf dem Mount Everest oder am Nordpol. Der Vektorpfeil zeigt an, dass Erdbeschleunigung und damit auch die Kraft eine Richtung besitzt.

> Die Einheit der Kraft $`F`$ ist das Newton: $`[F]=N`$. Für das Newton gilt: $`1N=1kg*\frac{m}{s^2}`$.

>
<h2 style="color:blue;">
Beispiel 3.1.1
</h2>
>
Peter stellt sich auf der Erde auf seine Personenwaage. Die Anzeige seiner Waage zeigt $`70,00kg`$ an. Mit einer Weltraummission fliegt er auf den Mond und stellt sich dort erneut auf seine Waage. Welches Gewicht zeigt die Waage auf dem Mond an?
Die Fallbeschleunigung beträgt auf der Erde $`9,81\frac{m}{s^2}`$ und auf dem Mond $`1,62m\frac{m}{s^2}`$.
Um diese Frage beantworten zu können, muss man sich klar machen, dass mit Hilfe einer Waage nicht die Masse eines Körpers gemessen wird, sondern die Kraft, mit der ein Körper von der Erde angezogen wird. Die Waage ist lediglich so kalibriert, dass das Gewicht direkt abgelesen werden kann. Legt man also eine Masse mit $`1kg`$ auf eine Waage, wird eigentlich ihre Gewichtskraft gemessen, in diesem Fall
>
<center>
$`F_{1kg,Erde}=1kg*9,81\frac{m}{s^2}=9,81N`$
</center>
>
Jedoch ist die Skala so angelegt, dass der Nutzer direkt die Masse, also Kilogramm ablesen kann. Auf Peter wirkt auf der Erde zwar eine Gewichtskraft von
>
<center>
$`F_{Peter,Erde}=70kg*9,81\frac{m}{s^2}=6871N`$
</center>
>
die Waage zeigt aber $`70kg`$ an.
Auf dem Mond erfährt Peter wegen der geringeren Fallbeschleunigung eine Gewichtskraft von:
>
<center>
$`F_{Peter,Mond}=70kg*1,62\frac{m}{s^2}=113N`$
</center>
>
Die Gewichtskraft auf dem Mond ist also wesentlich kleiner als auf der Erde. Die Waage zeigt bei dieser kleineren wirkenden Kraft auch einen geringeren Wert als auf der Erde an. Um den auf dem Mond angezeigten Wert der Masse zu erhalten, muss die Gewichtskraft auf dem Mond durch die Fallbeschleunigung der Erde geteilt werden, da die Waage auf diesen Wert kalibriert ist:
>
<center>
$`m_{angezeigt,Mond}=\frac{113N}{9,81\frac{m}{s^2}}=11,5kg`$
</center>
>
Man muss also unterscheiden zwischen der Masse eines Körpers und der Gewichtskraft. Die Masse ist eine unveränderliche Größe, eine Eigenschaft des Körpers, während die Gewichtskraft, die ein Körper erfährt, davon abhängt, wo sich ein Körper befindet.


Allgemein wird die Anziehung zwischen zwei Körpern durch das von Isaac Newton entdeckte Gravitationsgesetz beschrieben. Dieses Gesetz besagt, dass sich zwei Körper anziehen. Für die anziehende Kraft gilt folgender Zusammenhang:

<center>
$`F_G=G*\frac{m_1*m_2}{r^2}`$
</center>

mit:

$`m_1,m_2`$     : die sich anziehenden Massen; \
$`r`$           : der Abstand, in dem sich die Massen befinden; \
$`G`$           : Gravitationskonstante. Neben $`G`$ wir oft auch $`\gamma`$ verwendet.

Die Kraft wird hier wesentlich von den Massen der beiden Körper beeinflusst. Je schwerer ein Körper ist, desto größer wird die Kraft. Ist ein Körper sehr schwer, wie zum Beispiel die Erde, wird die Kraft sehr groß und kann leicht gemessen werden. Der Abstand zwischen den beiden Körpern wirkt sich ebenfalls stark auf die anziehende Kraft aus. Je größer der Abstand ist, desto kleiner ist die Anziehung zwischen den Körpern. Verdoppelt sich der Abstand zwischen den Körpern, sinkt die Kraft auf ein Viertel des ursprünglichen Wertes. Wichtig ist, dass die Kraft auf beide Körper wirkt. Die Erde zieht den fallenden Apfel genauso stark an, wie der Apfel die Erde. Die Masse des Apfels ist aber wesentlich kleiner als die der Erde. Deshalb ist die anziehende Wirkung des Apfels auf die Erde nicht zu bemerken. Der Apfel fällt deshalb auf die Erde und nicht die Erde auf den Apfel. \
Der Einfachheit halber werden die Größen Erdmasse, Gravitationskonstante und Erdradius zu den Fallbeschleunigungen zusammengefasst. Für die Erde erhält man den bekannten Wert für $`g`$. In der Tabelle wurden die Fallbeschleunigungen für Erde, Mond und Jupiter aus deren Massen und Radien bestimmt. Die berechneten Werte passen sehr gut zu den bekannten Werten. Abweichungen vom tatsächlichen Wert liegen zum Beispiel darin begründet, dass die Erde keine ideale Kugelform hat oder dass für die Berechnung beim Jupiter mit den Vielfachen von Erdmasse und Erdradius gerechnet wurde.

|        | Masse $`m`$    | Radius $`r`$   | Gravitationskonstante $`G`$ | Fallbeschleunigung $`g`$ | Tabellenwert |
|:-------|:--------------:|:--------------:|:---------------------------:|:------------------------:|:------------:|
| Erde   |$`5,974*10^{24}kg`$|$`6371 km`$|$`6,672*10^{-11}\frac{N*m^2}{kg^2}`$|$`\approx 9,820\frac{m}{s^2}`$|$`9,81\frac{m}{s^2}`$|
| Mond   |$`7,349*10^{22}kg`$|$`1738 km`$|$`6,672*10^{-11}\frac{N*m^2}{kg^2}`$|$`\approx 1,622\frac{m}{s^2}`$|$`1,62\frac{m}{s^2}`$|
| Jupiter|$`\approx 318*Erdmasse`$|$`\approx 12* Erdradius`$|$`6,672*10^{-11}\frac{N*m^2}{kg^2}`$|$`\approx 21,69\frac{m}{s^2}`$|$`23,1\frac{m}{s^2}`$|

Bei Kräften handelt es sich um vektorielle Größen. Sie sind gekennzeichnet durch einen Betrag und eine Richtung. Wirkt auf einen Körper eine Kraft, ändert sich der Bewegungszustand des Körpers. Wirkt keine Kraft, behält der Körper seinen Bewegungszustand bei. Ein Ball fliegt also ohne Einwirkung von Kräften mit unveränderter Geschwindigkeit immer weiter und ändert auch die Richtung nicht. In der Realität wirken jedoch Erdanziehung und Reibungskräfte, die den Ball zu Boden zwingen.

Zwei Kräfte sind vom Betrag gleich groß, wenn sie einen Körper gleich stark verformen oder seinen Bewegungszustand in der gleichen Weise verändern. Zwei vom Betrag gleich große Kräfte, die in die gleiche Richtung an einem Körper angreifen, addieren sich zur doppelten Kraft. Zeigen Sie in die entgegengesetzte Richtung, heben sie sich auf und der Körper bleibt in Ruhe. /
Allgemein gilt für eine Kraft das von Newton gefundene Grundgesetz der Dynamik:

<center>
$`F=m*a`$
</center>

oder in Vektorschreibweise 
$`\vec{F}=m*\vec{a}`$. \
Mit einer Kraft von $`F=1N`$ wird ein Körper der Masse  $`m=1kg`$ mit der Beschleunigung  $`a=1\frac{m}{s^2}`$ in einer Sekunde auf die Geschwindigkeit $`v=1\frac{m}{s}`$ beschleunigt.

----
<h2 style="color:red;">
Aufgabe 3.1.2
</h2>

Sie haben einen großen und einen kleinen Körper auf der Erde auf eine Balkenwaage gelegt. Die Waage befindet sich im Gleichgewicht. Was passiert, wenn Sie dieselbe Anordnung auf den Mond bringen?

**A.** Der Arm mit dem größeren Körper bewegt sich nach unten. \
**B.** Der Arm mit dem kleineren Körper bewegt sich nach unten. \
**C.** Nichts, warum sollte sich was ändern? \
**D.** Um das voraussagen zu können, braucht man mehr Informationen.

<details>
  <summary markdown="span">Lösung überprüfen</summary>

        Richtige Antworten: [A, B]

        Die Körper haben die gleiche Masse, sonst wäre die Waage auf der Erde nicht im Gleichgewicht. Auch auf dem Mond erfahren die beiden Körper aufgrund der gleichen Masse die gleiche Anziehung. Die Waage ist also auch auf dem Mond im Gleichgewicht.

</details>
----

[...]

----
<h2 style="color:red;">
Aufgabe 3.1.5
</h2>

Sie befinden sich auf einem unbekannten Planeten. Sie haben zufällig Ihre Waage dabei und stellen sich darauf. Im Vergleich zur Erde zeigt die Waage nur $`\frac{3}{4}`$ Ihres Gewichtes auf der Erde an. Wie groß ist die Fallbeschleunigung auf dem Planeten? Für die Fallbeschleunigung auf der Erde soll $`g=9,81\frac{m}{s^2}`$ gelten.

<label for="uinput">$`g_Planet =`$</label>
<input type="text" id="uinput" name="uinput">
$`\frac{m}{s^2}`$

<details>
  <summary markdown="span">Erklärung</summary>

    [Erklärung ...]

</details>
----

## Definition der Kraft

<figure>
  <video controls title="Video 27: Definition der Kraft (C).">
      <source src="img/markdown_video.mp4" type="video/mp4">
      Your browser does not support the video tag.
  </video>
</figure>


# Tests:

### Audio:

<figure>
  <audio controls title="GitLab test audio.">
      <source src="img/markdown_audio.mp3" type="audio/mpeg">
      Your browser does not support the audio element.
  </audio>
</figure>

### H5P:

<iframe src="https://h5p.org/h5p/embed/1023720" width="1090" height="282" frameborder="0" allowfullscreen="allowfullscreen" allow="geolocation *; microphone *; camera *; midi *; encrypted-media *"></iframe><script src="https://h5p.org/sites/all/modules/h5p/library/js/h5p-resizer.js" charset="UTF-8"></script>

<iframe src="https://h5p.org/h5p/embed/1023724" width="1090" height="244" frameborder="0" allowfullscreen="allowfullscreen" allow="geolocation *; microphone *; camera *; midi *; encrypted-media *"></iframe><script src="https://h5p.org/sites/all/modules/h5p/library/js/h5p-resizer.js" charset="UTF-8"></script>

<iframe src="https://h5p.org/h5p/embed/1023715" width="1090" height="306" frameborder="0" allowfullscreen="allowfullscreen" allow="geolocation *; microphone *; camera *; midi *; encrypted-media *"></iframe><script src="https://h5p.org/sites/all/modules/h5p/library/js/h5p-resizer.js" charset="UTF-8"></script>

### Footnote:

Sample footnote: [^1]

[^1]: This is how a footnote looks like.

### Checkbox:

- [x] Completed task
- [ ] Incomplete task
  - [ ] Sub-task 1
  - [x] Sub-task 2
  - [ ] Sub-task 3

### Lists

1. First
1. Second
   1. ordered
   1. sub-list
1. Third
   - unordered
   - sub-list
1. Last one

### Code-Blocks:

**In-line:** 

`Example [2+4=6]` code block in-line.

**Fenced:**
```python
def hello
    print("Hello World!")
end
```

### Backgorund coloring:

<span style="background-color: #FFFF00">This text is highlighted in yellow.</span>

Here is an example of <mark>highlighted text</mark> using the &lt;mark&gt; tag.

### Schriftgröße

<h1> Dieser Text ist riesig</h1>

<h3> Dieser Text ist mittelgroß</h3>

<h6> Dieser Text ist winzig
  <br>und geht über mehrere Zeilen
</h6>

### links

This is a [Link](https://gitlab.com/lennart-rosseburg/gitlabtestoer)
